-- Access the metastore

show databases ;

use wmf ; -- on production cluster

show tables ;

-- Access wmf tables

select uri_host from aqs_hourly where year = 2022 and month = 7 limit 10;
-- select uri_host from aqs_hourly where year = 2022 and month = 7 and day = 15 limit 10;
-- select uri_host from webrequest where year = 2022 limit 10;
-- select count(*) from aqs_hourly where year = 2022 and month = 7 and day = 15;