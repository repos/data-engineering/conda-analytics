"""
A pyspark job that tests several bits of functionality.
Run this job to test your pyspark distribution.
You can use this to test spark bundled in your conda dist env,
or to test a cluster installed spark distribution, all
depending on the arguments you provide to spark submit.

Extracted from https://gitlab.wikimedia.org/repos/data-engineering/example-job-project
"""

import time
from pprint import pprint

# Return the python executable and the PYTHONPATH used on a YARN PySpark worker:
def python_env_info() -> dict:
    import os, sys, platform
    return {
        'hostname': platform.node(),
        'python': sys.executable,
        'sys.path': sys.path,
        'SPARK_CONF_DIR': str(os.environ.get("SPARK_CONF_DIR"))
    }

def main() -> None:
    """
    Usage: spark-submit [...] pyspark_tester.py

    Runs various pyspark tests to verify spark deployments in in different clusters.
    """

    from pyspark.sql import SparkSession
    spark = SparkSession.builder.getOrCreate()
    sc = spark.sparkContext

    print('Driver python interpreter:')
    pprint(python_env_info())

    rdd = spark.sparkContext.parallelize([1], 1).mapPartitions(lambda p: [python_env_info()])
    r1 = rdd.collect()
    print('Executors python interpreter')
    pprint(r1)

    # Test numpy on remote workers:
    import numpy as np
    rdd = sc.parallelize([np.array([1,2,3]), np.array([1,2,3])], numSlices=2)
    r2 = rdd.reduce(lambda x,y: np.dot(x,y))
    print('Numpy result:')
    pprint(r2)

    # Test pyarrow on remote workers:
    import pyspark.sql.functions as F
    df = spark.range(0, 1000).withColumn('id', (F.col('id') / 100).cast('integer')).withColumn('v', F.rand())
    @F.pandas_udf(df.schema, F.PandasUDFType.GROUPED_MAP)
    def pandas_subtract_mean(pdf):
        return pdf.assign(v=pdf.v - pdf.v.mean())
    df2 = df.groupby('id').apply(pandas_subtract_mean)
    print('Pandas UDF result (pyarrow test):')
    df2.show()

    # Test Spark with Hive
    print('(hive) databases')
    db_df = spark.sql('show databases')
    db_df.show()

    # Test writing and reading from HDFS
    hdfs_file = f'hdfs:///tmp/pyspark_tester_show_databases.{time.time()}.csv'
    print (f'Writing csv to HDFS at {hdfs_file}')
    db_df.write.mode('overwrite').csv(hdfs_file)

    print (f'Reading csv from HDFS at {hdfs_file}')
    hdfs_df = spark.read.csv(hdfs_file)
    hdfs_df.show()

    # Test read some datasets
    spark.sql("select uri_host from wmf.aqs_hourly where year = 2022 and month = 7 limit 10;").show()

if __name__ == '__main__':
    main()
