# test cluster tests

* local[2]
    * versions OK
    * spark-shell OK
      ipython selected and it shouldn't
    * spark-sql OK
    * spark-submit OK
* yarn + client mode
    * spark-shell OK
    * spark-sql OK
    * pyspark
        * ipython not selected on driver, but detected when launching spark-shell
        * else is OK
    * spark-submit
        * jar OK
        * py OK
* yarn + cluster mode
    * spark-submit
        * jar OK
        * py OK
* yarn + skein
    * Need to set kerberos keytab
* Experimental Spark 3 Airflow launcher
  * OK, Python 3.10 selected

# Fixes

* deb postinst was not running.
* find-spark-home was setting PYSPARK_DRIVER_PYTHON and it should not.

# Airflow test on test cluster

* new configuration OK
* test hadoop library OK
* test aqs_hourly on Airflow OK
* test refinery with anomaly detection job on Airflow OK
* test Skein with Airflow OK

# Cloned conda env

* test cloned env without connexion OK
