// Check configuration
val arrayConfig=spark.sparkContext.getConf.getAll
for (conf <- arrayConfig)
    println(conf._1 +", "+ conf._2)

// => check the ui
// ssh -t -N -L4040:127.0.0.1:4040 an-test-client1001.e

// => check spark_home
//          master
//          deploy_mode
//          shuffle.service.enabled
//          authenticate


// Create a DF from a list
val columns = Seq("language","users_count")
val data = Seq(("Java", "20000"), ("Python", "100000"), ("Scala", "3000"))
val rdd = spark.sparkContext.parallelize(data)
val df = rdd.toDF()
df.printSchema()
df.show()

// Check scala version
rdd.partitions.size
val scalas = df.mapPartitions(s =>
    Array(util.Properties.versionString).iterator)
println(s"scala versions: ${scalas.collect().mkString(", ")}")

// Check hostnames
println(df.rdd.partitions.length)
val hosts = df.mapPartitions(s =>
    Array(java.net.InetAddress.getLocalHost().getHostName()).iterator)
println(s"Hostnames: ${hosts.collect().mkString(", ")}")

// Check hostnames with a bigger dataset
val df2 = spark.sql("select uri_host from wmf.aqs_hourly where year = 2022 and month = 7 limit 10000;")
println(df2.rdd.partitions.length)
val df3 = df2.repartition(20)
df3.count()
val hosts = df3.mapPartitions(s =>
    Array(java.net.InetAddress.getLocalHost().getHostName()).iterator)
println(s"Hostnames: ${hosts.collect().mkString(", ")}")

// Check conf dir var
import scala.collection.JavaConverters._
val confDirs = df.mapPartitions(s => Array(System.getenv().asScala.getOrElse("SPARK_CONF_DIR", "nope")).iterator)
println(s"conf dirs: ${confDirs.collect().mkString(", ")}")

// Test Hive datastore connection
val dbDf = spark.sql("show databases")
dbDf.show()

// Test writing and reading from HDFS
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
val t1 = LocalDateTime.now()
val t2 = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss").format(t1)
val hdfs_file = s"hdfs:///tmp/spark_tester_show_databases.${t2}.csv"
println(s"Writing csv to HDFS at ${hdfs_file}")
dbDf.write.mode("overwrite").csv(hdfs_file)

println(s"Reading csv from HDFS at ${hdfs_file}")
val hdfs_df = spark.read.csv(hdfs_file)
hdfs_df.show()

// inclusion of custom jar on all workers
// TODO
