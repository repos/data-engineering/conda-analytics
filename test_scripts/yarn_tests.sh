# Recipe to run Spark tests on the test cluster

# Note: conda-analytics has been installed on all machines on the cluster

# config an-test-client like local tests
# ...

# Move conf files to workers
scp -r an-test-client1001.e:~/spark3_conf spark3_conf
scp -r spark3_conf an-test-worker1001.e:~/spark3_conf
scp -r spark3_conf an-test-worker1002.e:~/spark3_conf
scp -r spark3_conf an-test-worker1003.e:~/spark3_conf
ssh an-test-worker1001.e
ssh an-test-worker1002.e
ssh an-test-worker1003.e
chmod +x spark3_conf/spark-env.sh

# Yarn, Client mode

export SPARK_CONF_DIR=~/spark3_conf

spark3-shell \
  --master yarn \
  --deploy-mode client \
  --name spark-shell-yarn-client-mode \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf

spark3-sql \
  --master yarn \
  --deploy-mode client \
  --name spark-sql-yarn-client-mode \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf

pyspark3 \
  --master yarn \
  --deploy-mode client \
  --name pyspark-yarn-client-mode \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf

spark3-submit \
  --master yarn \
  --deploy-mode client \
  --name spark-submit-yarn-client-mode-jar \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --class org.wikimedia.analytics.refinery.condaanalytics.Main \
  test.jar

spark3-submit \
  --master yarn \
  --deploy-mode client \
  --name spark-submit-yarn-client-mode-py \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  spark_submit_test.py

# Yarn, Cluster mode

export SPARK_CONF_DIR=~/spark3_conf

spark3-submit \
  --master yarn \
  --deploy-mode cluster \
  --name spark-submit-yarn-client-mode-jar \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --class org.wikimedia.analytics.refinery.condaanalytics.Main \
  test.jar

spark3-submit \
  --master yarn \
  --deploy-mode cluster \
  --name spark-submit-yarn-cluster-mode-py \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  spark_submit_test.py

# Skein Client

source /opt/conda-analytics/bin/activate
conda create --name spark-skein-test
conda activate spark-skein-test
conda install -c conda-forge skein
scp skein_test.yaml an-test-client1001.e:~/skein_test.yml

# test with custom-installed conda-env

/home/aqu/conda-analytics-0.0.9.dev/opt/conda-analytics/bin/pyspark \
  --master yarn \
  --deploy-mode client \
  --name pyspark-yarn-client-mode \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf

/home/aqu/conda-analytics-0.0.9.dev/opt/conda-analytics/bin/spark-submit \
  --master yarn \
  --deploy-mode client \
  --name spark-submit-yarn-client-mode-py \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  spark_submit_test.py

/usr/lib/airflow/lib/python3.7/site-packages/pyspark/bin/spark-submit \
  --master yarn \
  --deploy-mode client \
  --name spark-submit-yarn-client-mode-py \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  spark_submit_test.py

/usr/lib/airflow/lib/python3.7/site-packages/pyspark/bin/pyspark \
  --master yarn \
  --deploy-mode client \
  --name spark-submit-yarn-client-mode-py

# AQS test to test Spark Assembly

spark3-submit \
  --driver-cores 1 \
  --conf spark.executorEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --master yarn \
  --conf spark.dynamicAllocation.enabled=true \
  --conf spark.dynamicAllocation.maxExecutors=16 \
  --conf spark.shuffle.service.enabled=true \
  --conf spark.yarn.maxAppAttempts=1 \
  --conf spark.yarn.archive=hdfs:///user/spark/share/lib/spark-3.1.2-assembly.jar \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --name test-aqs-hourly-spark3-aqu \
  --class org.apache.spark.sql.hive.thriftserver.SparkSQLCLIDriver \
  --queue production \
  --deploy-mode client \
  -f hdfs://analytics-test-hadoop/wmf/refinery/current/hql/aqs/hourly.hql \
  -d source_table=wmf.webrequest \
  -d webrequest_source=test_text \
  -d destination_table=aqu.aqs_hourly \
  -d year=2022 \
  -d month=7 \
  -d day=6 \
  -d hour=12 \
  -d coalesce_partitions=8

spark3-submit \
  --driver-cores 1 \
  --conf spark.executorEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --master yarn \
  --conf spark.dynamicAllocation.enabled=true \
  --conf spark.dynamicAllocation.maxExecutors=16 \
  --conf spark.shuffle.service.enabled=true \
  --conf spark.yarn.maxAppAttempts=1 \
  --conf spark.yarn.archive=/homerrr/aqu/spark-3.1.2-assembly.zip \
  --conf spark.yarn.appMasterEnv.SPARK_CONF_DIR=/home/aqu/spark3_conf \
  --name test-aqs-hourly-spark3-aqu \
  --class org.apache.spark.sql.hive.thriftserver.SparkSQLCLIDriver \
  --queue production \
  --deploy-mode client \
  -f hdfs://analytics-test-hadoop/wmf/refinery/current/hql/aqs/hourly.hql \
  -d source_table=wmf.webrequest \
  -d webrequest_source=test_text \
  -d destination_table=aqu.aqs_hourly \
  -d year=2022 \
  -d month=7 \
  -d day=6 \
  -d hour=12 \
  -d coalesce_partitions=8
