"""
A pyspark job that tests several bits of functionality.
Run this job to test your pyspark distribution.
You can use this to test spark bundled in your conda dist env,
or to test a cluster installed spark distribution, all
depending on the arguments you provide to spark submit.

Extracted from https://gitlab.wikimedia.org/repos/data-engineering/example-job-project
"""

from typing import List

import time
from pprint import pprint


# Return the python executable and the PYTHONPATH used on a YARN PySpark worker:
def python_env_info() -> dict:
    import os, sys, platform
    return {
        'hostname': platform.node(),
        'python': sys.executable,
        'sys.path': sys.path,
        'SPARK_CONF_DIR': str(os.environ.get("SPARK_CONF_DIR"))
    }

# Test Python requests library.
# Requires that CA certs are loaded properly, probably via REQUESTS_CA_BUNDLE env var.
# Also requires that http_proxy, https_proxy, and no_proxy settings are correct.
def http_request(url) -> dict:
    import requests
    try:
        return {
            'url': url,
            'response_status': str(requests.get(url).status_code)
        }
    except Exception as e:
        print(f'Caught exception when requesting {url}', e)
        return {
            'url': url,
            'exception': str(e),
        }


def do_wmf_http_requests() -> List[str]:
    external_url = "https://en.wikipedia.org"
    internal_url = "https://ms-fe.svc.eqiad.wmnet/wikipedia/commons/thumb/a/a8/Tour_Eiffel_Wikimedia_Commons.jpg/100px-Tour_Eiffel_Wikimedia_Commons.jpg"
    return [http_request(external_url), http_request(internal_url)]


print('Driver python interpreter:')
pprint(python_env_info())

rdd = spark.sparkContext.parallelize([1], 1).mapPartitions(lambda p: [python_env_info()])
r1 = rdd.collect()
print('Executors python interpreter')
pprint(r1)

    # Test numpy on remote workers:
import numpy as np
rdd = sc.parallelize([np.array([1,2,3]), np.array([1,2,3])], numSlices=2)
r2 = rdd.reduce(lambda x,y: np.dot(x,y))
print('Numpy result:')
pprint(r2)

    # Test pyarrow on remote workers:
import pyspark.sql.functions as F
df = spark.range(0, 1000).withColumn('id', (F.col('id') / 100).cast('integer')).withColumn('v', F.rand())
@F.pandas_udf(df.schema, F.PandasUDFType.GROUPED_MAP)
def pandas_subtract_mean(pdf):
    return pdf.assign(v=pdf.v - pdf.v.mean())
df2 = df.groupby('id').apply(pandas_subtract_mean)
print('Pandas UDF result (pyarrow test):')
df2.show()

# Test driver HTTP requests
h1 = do_wmf_http_requests()
print('Driver http request results:')
pprint(h1)

# Test executor HTTP requests
rdd = spark.sparkContext.parallelize(range(2), 2).mapPartitions(lambda p: do_wmf_http_requests())
h2 = rdd.collect()
print('Executors http request results:')
pprint(h2)

# Test Spark with Hive
print('(hive) databases')
db_df = spark.sql('show databases')
db_df.show()

# Test writing and reading from HDFS
hdfs_file = f'hdfs:///tmp/pyspark_tester_show_databases.{time.time()}.csv'
print (f'Writing csv to HDFS at {hdfs_file}')
db_df.write.mode('overwrite').csv(hdfs_file)

print (f'Reading csv from HDFS at {hdfs_file}')
hdfs_df = spark.read.csv(hdfs_file)
hdfs_df.show()

# Test read some datasets
spark.sql("select uri_host from wmf.aqs_hourly where year = 2022 and month = 7 limit 10;").show()