package org.wikimedia.analytics.refinery.condaanalytics

import org.apache.spark.sql._
import scala.util

object Main extends App { 

    println("Hello, world") 
    // val spark = SparkSession.active
    val spark = SparkSession.builder()
    // .appName("test1")
    // .master("yarn")
    .enableHiveSupport()
    .getOrCreate()
    import spark.implicits._

    // Test retrieve some data from HDFS
    val df = spark.sql("select uri_host from wmf.aqs_hourly where year = 2022 and month = 7 and day = 1 limit 10;")
    df.show()

    // Print Scala versions on each partitions
    val scalas = df.mapPartitions(s =>
        Array(util.Properties.versionString).iterator)
    println(s"scala versions: ${scalas.collect().mkString(", ")}")
}
