# Recipe to run Spark tests on the test cluster


# Setup conf if not set in /etc/spark3/conf
cp -R /etc/spark3/conf ~/spark3_conf
# update spark-env.sh with the target version
vim spark3_conf/spark-env.sh
export SPARK_CONF_DIR=~/spark3_conf

# Test bin files
spark3-shell --version
spark3-sql --version
spark3-submit --version
pyspark3 --version

# Test Spark 3 in local mode
spark3-shell \
  --master local[2] \
  --name spark-shell-local-mode

spark3-sql \
  --master local[2] \
  --name spark-sql-local-mode

spark3-submit \
  --master local[2] \
  --name spark-submit-local-mode \
  -h

# Move to launcher some test scripts to submit
sbt package
scp test_scripts/spark_submit_test.py an-test-client1001.e:~/
scp test_scripts/Projects/CondaAnalyticsTeststarget/scala-2.12/myproject_2.12-1.0.jar
  \ an-test-client1001.e:~/test.jar