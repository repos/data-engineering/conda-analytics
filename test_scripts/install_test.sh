# Recipe to install a custom version of conda-analytics on the test cluster

# on an-test-{client1001 | worker100{1,2,3}}:
wget -O conda-analytics-0.0.9.dev0_amd64.deb https://gitlab.wikimedia.org/repos/data-engineering/conda-analytics/-/package_files/727/download
dpkg -x conda-analytics-0.0.9.dev0_amd64.deb conda-analytics-0.0.9.dev
conda-analytics-0.0.9.dev/opt/conda-analytics/bin/python conda-analytics-0.0.9.dev/opt/conda-analytics/bin/conda-unpack
head conda-analytics-0.0.9.dev/opt/conda-analytics/bin/ipython
vim ~/spark3_conf/spark-env.sh
