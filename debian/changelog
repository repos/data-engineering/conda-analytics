conda-analytics (0.0.38) bullseye-wikimedia; urgency=medium

  * Install Wmfdata-Python from its new location on Wikimedia GitLab
  Fixes: #T304544

 -- Ben Tullis <btullis@wikimedia.org>  Wed, 12 Feb 2025 09:54:21 +0000

conda-analytics (0.0.37) bullseye-wikimedia; urgency=medium

  * Upgrade miniforge to version 24.11.2
  * Add constraints to prevent the use of anaconda default channels
  * Update transient dependencies

 -- Ben Tullis <btullis@wikimedia.org>  Fri, 17 Jan 2025 09:53:45 +0000

conda-analytics (0.0.36) bullseye-wikimedia; urgency=medium

  * Switch from miniconda to miniforge to build the package
  * Stop using the classic solver in conda-analytics-clone
  * Remove the strict channel priority from condarc

 -- Balthazar Rouberol <brouberol@wikimedia.org>  Thu, 19 Sep 2024 16:06:19 +0100

conda-analytics (0.0.35) bullseye-wikimedia; urgency=medium

  * Update conda-analytics-clone to use whoami instead of $USER

 -- Ben Tullis <btullis@wikimedia.org>  Thu, 04 Jul 2024 16:06:19 +0100

conda-analytics (0.0.34) bullseye-wikimedia; urgency=medium

  * Remove stray trailing brace from the cloned environment name.
    Fixes: #T369210
  * Update conda-pinned with more required package specs.
    Fixes: #T369240
  * Update transient dependencies.

 -- Ben Tullis <btullis@wikimedia.org>  Thu, 04 Jul 2024 12:16:11 +0100

conda-analytics (0.0.33) bullseye-wikimedia; urgency=medium

  * Fix a crash in conda-analytics-clone caused by an unset variable

 -- Balthazar Rouberol <brouberol@wikimedia.org>  Wed, 03 Jul 2024 10:00:00

conda-analytics (0.0.32) bullseye-wikimedia; urgency=medium

  * Upgrade conda version

 -- Steve Munene <smunene@wikimedia.org>  Mon, 17 June 2024 03:00:00

conda-analytics (0.0.31) bullseye-wikimedia; urgency=medium

  * Manually copy pinned file to cloned environment

 -- Steve Munene <smunene@wikimedia.org>  Wed, 30 May 2024 10:00:00

conda-analytics (0.0.30) bullseye-wikimedia; urgency=medium

  * Introduce the conda pinned file used to pin versions
  * Introduce the --pinned tag in the conda-analytics-clone to ensure new clones have the pinned file
  * Updating transient packages numpy and pandas

 -- Steve Munene <smunene@wikimedia.org>  Wed, 16 May 2024 09:37:56

conda-analytics (0.0.29) bullseye-wikimedia; urgency=medium

  * Update image to bullseye:20240414

 -- Steve Munene <smunene@wikimedia.org>  Wed, 17 April 2024 07:56:56 +0000

conda-analytics (0.0.28) buster-wikimedia; urgency=medium

  * Update wmfdata to version 2.3.0 to support a presto-coordinator CNAME

 -- Ben Tullis <btullis@wikimedia.org>  Wed, 31 Jan 2024 15:27:42 +0000

conda-analytics (0.0.27) buster-wikimedia; urgency=medium

  * Update conda to 23.10.0 and remove specific pinning of mamba packages
  * Add some additional tests during the build process
  * Update transient dependencies
  * Update pyarrow to version 9.0.0

 -- Ben Tullis <btullis@wikimedia.org>  Thu, 07 Dec 2023 14:59:12 +0000

conda-analytics (0.0.26) buster-wikimedia; urgency=medium

  * Update the version on wmfdata-python to pick up the new CA settings
  * Update transient dependencies

 -- Ben Tullis <btullis@wikimedia.org>  Wed, 06 Dec 2023 18:37:37 +0000

conda-analytics (0.0.25) buster-wikimedia; urgency=medium

  * Update the version of miniconda used to perform the build to version 23.10.0

 -- Ben Tullis <btullis@wikimedia.org>  Tue, 05 Dec 2023 11:18:46 +0000

conda-analytics (0.0.24) buster-wikimedia; urgency=medium

  * Remove the spark shuffler service jar file

 -- Ben Tullis <btullis@wikimedia.org>  Mon, 23 Oct 2023 12:34:48 +0100

conda-analytics (0.0.23) buster-wikimedia; urgency=medium

  * Ensure that the limbmamba solver uses the conda-forge channel
  * Bump wmfdata-python to version 2.0.1
  * Update conda to version 23.7.4 and conda-libmamba-solver to 23.7.0
  * Update transient dependencies

 -- Ben Tullis <btullis@wikimedia.org>  Fri, 22 Sep 2023 17:02:20 +0100

conda-analytics (0.0.22) buster-wikimedia; urgency=medium

  * Ensure that the libmamba minor version matches that of conda

 -- Ben Tullis <btullis@wikimedia.org>  Fri, 15 Sep 2023 11:02:01 +0100

conda-analytics (0.0.21) buster-wikimedia; urgency=medium

  * Install the libmamba solver so that it can be used

 -- Ben Tullis <btullis@wikimedia.org>  Thu, 14 Sep 2023 15:56:01 +0100

conda-analytics (0.0.20) buster-wikimedia; urgency=medium

  * Pin the version of sqlalchemy to <2.0 for jupyterhub

 -- Ben Tullis <btullis@wikimedia.org>  Wed, 13 Sep 2023 18:43:05 +0100

conda-analytics (0.0.19) buster-wikimedia; urgency=medium

  * Update miniconda to version 23.5.2
  * Update transient dependencies
  * Startusing the mamba solver by default #T337258

 -- Ben Tullis <btullis@wikimedia.org>  Wed, 06 Sep 2023 16:52:00 +0100

conda-analytics (0.0.18) buster-wikimedia; urgency=medium

  * Use conda-environment.lock.yml when creating the environment

 -- Ben Tullis <btullis@wikimedia.org>  Thu, 25 May 2023 20:50:02 +0100

conda-analytics (0.0.17) buster-wikimedia; urgency=medium

  * Correct the file name of the spark3 shuffler jar.

 -- Ben Tullis <btullis@wikimedia.org>  Mon, 22 May 2023 20:52:56 +0100

conda-analytics (0.0.16) buster-wikimedia; urgency=medium

  * Correct the jar file used for the spark3 shuffler service.

 -- Ben Tullis <btullis@wikimedia.org>  Mon, 22 May 2023 12:35:08 +0100

conda-analytics (0.0.15) buster-wikimedia; urgency=low

  * Adds the spark3 yarn shuffle service jar to the environment #T332765
  * Version 0.0.14 was skipped due to an issue with the release process

 -- Ben Tullis <btullis@wikimedia.org>  Mon, 15 May 2023 11:28:22 +0100

conda-analytics (0.0.13) buster-wikimedia; urgency=low

  * Adds support for Iceberg 1.2.1 as a runtime dependency of Spark 3.1.2.

 -- Xabriel J Collazo Mojica (WMF) <xcollazo@wikimedia.org>  Mon, 1 May 2023 12:00:00 +0200

conda-analytics (0.0.12) buster-wikimedia; urgency=low

  * Updates wmfdata to version 2.0.0.
  * Various bug fixes regarding cloned conda environment stability and reproducibility.

 -- Xabriel J Collazo Mojica (WMF) <xcollazo@wikimedia.org>  Wed, 23 Nov 2022 12:00:00 +0200

conda-analytics (0.0.11) buster-wikimedia; urgency=low

  * Add support for JupyterHub and JupyterLab.
  * Add helper scripts to clone, list, activate and deactivate.

 -- Xabriel J Collazo Mojica (WMF) <xcollazo@wikimedia.org>  Thu, 27 Oct 2022 12:00:00 +0200

conda-analytics (0.0.10) buster-wikimedia; urgency=low

  * Add pkgs dir for cloning without touching internet
  * Add process to clean the pkgs dir in some environments

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 5 Sep 2022 12:00:00 +0200

conda-analytics (0.0.9) buster-wikimedia; urgency=low

  * Fix postinst in Docker script
  * Avoid setting PYSPARK_DRIVER_PYTHON in find-spark-home

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 5 Sep 2022 12:00:00 +0200

conda-analytics (0.0.8) buster-wikimedia; urgency=low

  * Set spark version to 3.1.2
  * Fix conda-pack paths
  * Install pyspark with conda
  * Fixing versions of conda dependencies: numpy, pandas, & pyarrow

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 7 Jul 2022 12:00:00 +0200

conda-analytics (0.0.7) buster-wikimedia; urgency=low

  * test package upgrade

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 7 Jul 2022 12:00:00 +0200

conda-analytics (0.0.6) buster-wikimedia; urgency=low

  * Rename the repo to conda-analytics

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 7 Jul 2022 12:00:00 +0200

conda-analytics (0.0.5) buster-wikimedia; urgency=low

  * Refactoring

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 7 Jul 2022 12:00:00 +0200

conda-analytics (0.0.4) buster-wikimedia; urgency=low

  * Multiple fixes

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 27 Jun 2022 12:00:00 +0200

conda-analytics (0.0.2) buster-wikimedia; urgency=low

  * Fix internal conda env paths

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 8 Jun 2022 19:00:00 +0200

conda-analytics (0.0.2) buster-wikimedia; urgency=low

  * Fix bin links

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 8 Jun 2022 19:00:00 +0200

conda-analytics (0.0.1) buster-wikimedia; urgency=low

  * Initial release

 -- Aqu (WMF) <aquhen@wikimedia.org>  Mon, 30 May 2022 19:15:40 +0200
