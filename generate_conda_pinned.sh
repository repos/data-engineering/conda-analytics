#!/bin/bash

input_filename="conda-environment.yml"
output_filename="conda-pinned"

# Check if the input file exists
if [ ! -f "$input_filename" ]; then
    echo "Input file $input_filename not found."
    exit 1
fi

# Check if pinned exists within the file then,
# for every line tagged 'pinned', strip the first few characters and save to the output file
if grep -q "#pinned" "$input_filename"; then
    sed -En "s/^[ \-]*([[:alnum:][:punct:]]*) *\#pinned$/\1/p" "$input_filename" > "$output_filename"
    echo "Pinned versions written to $output_filename"
else
    echo " #pinned keyword not found in $input_filename"
    exit 1
fi
