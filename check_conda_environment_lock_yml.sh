#!/usr/bin/env bash

# Script to check that:
# * the conda-environment.lock.yml can really be used to produce a conda environment.
# * the new environment can itself be cloned
# * that conda and the conda-libmamba-solver can be installed to this environment cleanly

set -e
set -x

now="$(date +%s)"
env_name="tmp_check_env_${now}"
lock_file="conda-environment.lock.yml"

# Create the new environment
conda env create --name $env_name -f $lock_file

# Get the versions of conda and conda-libmamba-solver that are installed to the new environment
CONDA_VERSION=$(conda list --name $env_name -f conda --json | jq -r .[].version)
CONDA_LIBMAMBA_SOLVER_VERSION=$(conda list --name $env_name -f conda-libmamba-solver --json | jq -r .[].version)

# Create a clone of the environment
conda create --offline --clone $env_name --name ${env_name}_clone

# Make sure that we can install conda and conda-libmamba-solver from the local package cache
conda install --yes --offline --freeze-installed --name ${env_name}_clone --solver classic conda=${CONDA_VERSION} conda-libmamba-solver=${CONDA_LIBMAMBA_SOLVER_VERSION}

# Tidy up
conda env remove --name $env_name
conda env remove --name ${env_name}_clone

set +x

echo "An environment could be created from $lock_file."
echo "It could also clone itself and then install conda."
