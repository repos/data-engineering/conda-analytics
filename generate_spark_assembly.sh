# SPARK ASSEMBLY GENERATION

# This recipe is the create of the assembly used by Spark to distribute its jars to the YARN
# workers.

# You could run this recipe as an SRE on an-{test-,}launcher, or any other machine
# with the hdfs user.

ssh an-launcher1002.eqiad.wmnet
# TODO: update the version
export SPARK_VERSION=3.1.2
export ASSEMBLY_FILE=spark-$SPARK_VERSION-assembly.jar
export HDFS_DIR=/user/spark/share/lib/
# `jar 0` stands for no compression. Thus the `.jar` extension; not `.zip`.
# (The compression gain is minimal)
jar cv0f \
  $ASSEMBLY_FILE \
  -C /opt/conda-analytics/lib/python3.10/site-packages/pyspark/jars/ .
# $HDFS_DIR is owned by spark, so the below put should create the file with ownership hdfs:spark.
sudo -u hdfs hdfs dfs -put $ASSEMBLY_FILE "${HDFS_DIR}${ASSEMBLY_FILE}"
sudo -u hdfs hdfs dfs -chmod +r "${HDFS_DIR}${ASSEMBLY_FILE}"
