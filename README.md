# The `conda-analytics` environment

This project creates a minimal base Conda environment used across the [Analytics cluster](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Cluster). It currently contains:

- Spark 3.1.2 (via `pyspark`)
- Python 3.10
- JupyterHub 1.5.0 (enables running multi-user JupyterLab instances)
- JupyterLab 3.4.8 (web-based interactive development environment for notebooks)
- Iceberg 1.2.1 (via runtime dependency installed jointly with `pyspark`)
- The YARN shuffler jar file for Spark version 3.1.2


## Adding or upgrading included packages

If you want to add a Conda or Pip package cluster-wide, add the package with its version number in `conda-environment.yml`.

Then recreate the `conda-environment.lock.yml` file, which specifies all the packages that will be present in the environment:
```bash
./generate_conda_environment_lock_yml.sh
```

Then check the changes in your new env file and test it:
```bash
git diff conda-environment.lock.yml
# Make sure you don't git add regressions (like removing gitlab urls).
./check_conda_environment_lock_yml.sh
```

Then regenerate the pinned file, which instructs Conda that the listed versions should never be changed, based on the `#pinned` comments in `conda-environment.yml`. The file will be placed in the `conda-meta` directory.

```bash
./generate_conda_pinned.sh
```

Check the changes in your new pinned file and proceed to commit:
```bash
git diff conda-pinned
```

## Deployment

### Prepare the build

* Find new version number in .bumpversion.cfg (just remove the .dev to get it).
* Create a new entry in the changelog
  Note: The Debian changelog should respect the Debian changelog conventions.
* Commit and push

### Building `conda-analytics`
This repo includes a [`Dockerfile`](https://gitlab.wikimedia.org/repos/data-engineering/conda-analytics/-/blob/main/docker/Dockerfile) that builds the `conda-analytics` debian package.

#### Building with Gitlab CI

This GitLab project is set up to use a [custom gitlab runner](https://phabricator.wikimedia.org/T321736) that builds the debian package directly from our `Dockerfile`.

There are two pipelines available, and you should typically run them in order:
* `trigger_release`: Run this on the `main` branch. It will bump the version of the artifact (I.e. from `0.0.12.dev` to `0.0.12`).
* `publish_package_with_docker`: Run this on the target git tag (I.e. git tag `v0.0.12`). Make sure you define the `PACKAGE_VERSION` variable (I.e for tag `v0.0.12`, set `PACKAGE_VERSION=0.0.12`). It will build and persist the debian package to this project's [package registry](https://gitlab.wikimedia.org/repos/data-engineering/conda-analytics/-/packages).

#### Build manually with docker

You can test changes to `conda-analytics` by building it locally without Gitlab CI.

Update the version number inside the `docker/Dockerfile`, then run the following from the root of this repository:
```bash

# Build the docker images.
# This will build each multistage build target in parallel:
#
# - conda_dist
# - build_deb
# - test_deb
#
# This speeds up the build, and allows each stage to be developed / debugged individually.
docker build --platform linux/amd64 -f docker/Dockerfile -t conda-analytics .

# If you want to limit the build to just one of the build targets, use the --target opt, e.g.
# Do this before running the following docker run command if you want to run a shell
# on one of the earlier stage builds.
docker build --target conda_dist --platform linux/x86_64 -f docker/Dockerfile -t conda-analytics .

# To get a shell on one the final target
docker run --platform linux/amd64 --memory="4g" --cpus="2.0" --rm -it conda-analytics
```

To copy the built .deb package from the image to your local host:

```bash
PACKAGE_VERSION=0.0.7
docker create --platform linux/amd64 -ti --name conda-analytics-with-deb conda-analytics bash
docker cp conda-analytics-with-deb:/srv/conda-analytics/conda-analytics-${PACKAGE_VERSION}_amd64.deb ./
docker rm -f conda-analytics-with-deb
```

### Add a new version of the debian package to apt.wikimedia.org

You can do this if you have rights to push to [our APT repository](https://wikitech.wikimedia.org/wiki/APT_repository).

First, fetch the .deb file. Find the url on the [package page](https://gitlab.wikimedia.org/repos/data-engineering/conda-analytics/-/packages), or copy it out of the docker build image as described above.

You can now add these files into apt.wikimedia.org using [reprepro includedeb](http://wikitech.wikimedia.org/view/Reprepro):

```bash
curl -o pconda-analytics-<version>_<arch>.deb <http-link-to-deb-package>
reprepro -C main includedeb <file>.deb
```

## Usage

After installing the .deb, the environment will be available at `/opt/conda-analytics`.

Conda activate the environment (read-only):
```
source /opt/conda-analytics/bin/activate
```

Or, clone the environment:

```
/opt/conda-analytics/bin/conda clone --name 'conda-analytics-clone0' --clone /opt/conda-analytics
```

### Spark
Launch Spark CLIs with:
* `spark3-shell`
* `spark3-sql`
* `spark3-submit`
* `pyspark3`

You may override the provided python (i.e. `python3.10`) with:
* `spark.pyspark.driver.python`
* `spark.pyspark.python`
(or with env vars: `PYSPARK_PYTHON` / `PYSPARK_DRIVER_PYTHON`)

If installed by Puppet in WMF production, `PYSPARK_PYTHON` and `PYSPARK_DRIVER_PYTHON` should be configured properly for you in `/etc/spark3/conf/spark-env.sh`.
